/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
} from 'react-native';

import FoneNumber from './fone_number';
import Employe from './list_employes';
import PinCode from './pin';
import Register from './register';
import MenuList from './list';

//this is for props and state
//class First extends Component {
  //constructor(props){
    //super(props);
    //this.state = {showText: true};

    //setInterval(() =>{
      //this.setState({showText: !this.state.showText});
    //},1000);
  //}

  //render(){
    //let sms = this.state.showText? this.props.name : '';
    //return (
      //<Text>{sms}</Text>
    //);
  //}
//}

//this one for ListView
//class ListViewProject extends Component{
  //constructor(props){
    //super(props)
    //const lv = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    //this.state = {
      //dataSource: lv.cloneWithRows(['Alpha','Bobo','Syta','Tidiane'])
    //};
  //}

  //render(){
    //return(
      //<View >
        //<ListView 
         // dataSource={this.state.dataSource}
          //renderRow={(rowData) => <Text>{rowData}</Text>} />
      //</View >
    //)
  //}
//}

//using button and tap event
//class MyButton extends Component{
  //_onPressButton(){
    //console.log("You tapped the button!");
  //}

  //render(){
    //return(
      //<TouchableHighlight onPress={this._onPressButton}>
        //<Text>valider</Text>
      //</TouchableHighlight>
    //)
  //}
//}


//this is for TextInput
//class PizzaProject extends Component {
  //constructor(props){
    //super(props);
    //this.state = {text: ''};
  //}

  //render(){
    //return (
      //<View style={{padding:10}}>
        //<TextInput
          //style = {{height: 40}}
          //placeholder='Votre texte ici'
          //onChangeText = {(text) => this.setState({text: text})}/>

        //<Text style = {styles.background}>
          //{this.state.text.split(' ').map((mot)=> mot && '🍕').join(' ')}
        //</Text>
      //</View>
    //);
  //}
//}
// <MyButton style={{backgroundColor: 'black', color: 'white'}} />
export default class SecondProject extends Component {
  
  render() {
    return (
        <MenuList />
    );
  }
}

AppRegistry.registerComponent('SecondProject', () => SecondProject);
