import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Navigator,
    ListView,
    NativeModules
} from 'react-native';
//import { COLOR, ThemeProvider,Toolbar, Button } from 'react-native-material-ui';
import { SideMenu, List, ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';

const list = [
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  }
];

class App extends Component {

    constructor (props) {
        super(props);
        const lv = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: lv.cloneWithRows([
                {
                    name: 'Amy Farha',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                    subtitle: 'Vice President'
                },
                {
                    name: 'Chris Jackson',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
                    subtitle: 'Vice Chairman'
                }
            ])
        };
    }

    renderRow (rowData, sectionID) {
        return (
            <ListItem
                roundAvatar
                key={sectionID}
                title={rowData.name}
                subtitle={rowData.subtitle}
                avatar={{uri:rowData.avatar_url}}
            />
        )
    }

    render () {
        return (
            <List>
                <ListView
                    renderRow={this.renderRow}
                    dataSource={this.state.dataSource}
                />
            </List>
        )
    }

}

export default class MenuList extends Component {

    constructor () {
        super()
        this.state = {
            isOpen: false
        }
        this.toggleSideMenu = this.toggleSideMenu.bind(this)
    }

    onSideMenuChange (isOpen) {
        this.setState({
            isOpen: isOpen
        })
    }

    toggleSideMenu () {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    render () {
        const MenuComponent = (
            <View style={{flex: 1, backgroundColor: '#ededed', paddingTop: 50}}>
                <List containerStyle={{marginBottom: 20}}>
                    {
                        list.map((l, i) => (
                        <ListItem
                            roundAvatar
                            onPress={() => console.log('Pressed')}
                            avatar={l.avatar_url}
                            key={i}
                            title={l.name}
                            subtitle={l.subtitle}
                        />
                        ))
                    }
                </List>
            </View>
        )

        return (
            <SideMenu
                isOpen={this.state.isOpen}
                onChange={this.onSideMenuChange.bind(this)}
                menu={MenuComponent}>
                <App toggleSideMenu={this.toggleSideMenu.bind(this)} />
            </SideMenu>
        )
    }
    
}




const styles = StyleSheet.create({
  container: {
    //flexDirection: 'row', pour aligner horizontalement 
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  heading: {
    fontSize: 28,
    textAlign: 'center',
    marginTop: 10,
    color: '#F5FCFF',
    fontWeight: 'bold'
  },
  body: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    width: 200,
  },
  tete: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#0050ef',
  },
  text: {
    justifyContent: 'space-between',
    textAlign: 'center',
    marginBottom: 5,
    //color: '#F5FCFF'
  },
  nom:{
      color: 'black'
  },
  image: {
    alignSelf: 'center',
  },
  input: {
    color: '#F5FCFF',
    borderWidth: 1,
    borderColor: '#F5FCFF'
  },
  employe: {
    marginLeft: 20,
    marginRight: 50,
    fontSize: 20,
    color: '#F5FCFF'
  }
});