import React, { Component,} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
} from 'react-native';

//import TelephoneInput from 'react-native-telephone-input';
//import PhoneInput from 'react-native-phone-input';


export default class FoneNumber extends Component {

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.heading}>Numero de téléphone</Text>
                <View style={styles.phone}>
                  <Text style={styles.text}>
                      Saisissez le numéro de téléphone.Il s'agit du numéro de téléphone
                      utilisé lors de la création de votre compte.
                  </Text>
                </View>
                <View style={styles.phone}>
                  <TextInput 
                    ref="9"
                    style={styles.input} 
                    placeholder='664 23 23 23' 
                    keyboardType="numeric"
                  />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    //flexDirection: 'row', pour aligner horizontalement
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#007db5',
  },
  heading: {
    fontSize: 28,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 30,
    color: '#F5FCFF',
    fontWeight: 'bold'
  },
  text: {
    justifyContent: 'center',
    width: 300,
    marginBottom: 50,
    color: '#F5FCFF'
  },
  phone: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  input:{
    //height: 20,
    width: 300,
    borderBottomWidth: 2,
    borderColor: '#F5FCFF',
    fontSize: 28
  }
  
});