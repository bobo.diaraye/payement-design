import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Dimensions,
    KeyboardAvoidingView,
    TouchableHighlight
} from 'react-native';

import CodePin from 'react-native-pin-code';

//import ReactDOM from 'react-dom';
//import PinInput from 'react-pin-input/src'
//import PinInput from 'react-pin-input';

//import reactPin from 'react-pin';
//import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
//import classNames from 'classnames';
//import { assert } from 'chai'; <PinInput length={4} secret onChange={(value, index) => {}} onComplete={(value, index) => {}} />

//const Pin = reactPin({ React, ReactCSSTransitionGroup, classNames, assert });

const {height, width} = Dimensions.get('window');

export default class PinCode extends Component {

  constructor() {
    super();

    this.state = {
      displayCodePin : true,
      success        : ''
    }
  }

  onSuccess = () => {

    this.ref.focus(1);

    this.setState({
      displayCodePin : false,
      success        : 'A success message :)'
    });

  }


    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.heading}>Nouveau PIN</Text>

                <Text style={styles.text}>
                    Choisir un nouveau PIN
                </Text>
                <Text style={styles.instructions}> 
                  {this.state.success} 
                </Text>
                
                <KeyboardAvoidingView behavior={'position'} keyboardVerticalOffset={-30} contentContainerStyle={styles.avoidingView}>
                  <CodePin 
                    ref={ref => this.ref = ref}
                    code="1234"
                    success={this.onSuccess}            
                  />
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    //flexDirection: 'row', pour aligner horizontalement
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#007db5',
  },
  heading: {
    fontSize: 28,
    textAlign: 'center',
    marginTop: 10,
    color: '#FFF',
    fontWeight: 'bold'
  },
  text: {
    justifyContent: 'space-between',
    textAlign: 'center',
    marginBottom: 5,
    color: '#FFF'
  },
  red: {
    color: 'red',
  },
  blue: {
    color: 'blue',
    fontSize: 10,
    fontWeight: 'bold',
  },
  background: {
    backgroundColor: '#F5000F',
    padding: 10,
    fontSize: 42,
  },
  avoidingView: {
    borderRadius  : 10,
    height        : 150,
    marginTop     : 50,
    width         : width - 30,
  }
});