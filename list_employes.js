import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Navigator,
    PropTypes,
    NativeModules
} from 'react-native';
import { COLOR, ThemeProvider,Toolbar, Button } from 'react-native-material-ui';
import Icon from 'react-native-vector-icons/FontAwesome';

const uiTheme = {
    palette: {
        primaryColor: COLOR.blue500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
    button: {
        container: {
            marginTop: 50,
        }
    }
};

export default class Employe extends Component {

    

    render(){
        return(
            <View style={styles.container}>
                <ThemeProvider uiTheme={uiTheme}>
                        <Toolbar
                            leftElement="menu"
                            centerElement="Searchable"
                            searchable={{
                            autoFocus: true,
                            placeholder: 'Search',
                            }}
                        />
                </ThemeProvider>
                <View style={styles.body}>
                    <Text style={styles.text}>
                        Creez votre premier Employé.
                        Creer des Employés permet à vos Employés de pouvoir utiliser
                        Smart Pay sans qu'il ne puissent acceder au statistic globales
                        des ventes ainsi que les données privées. 
                   </Text>
                    <ThemeProvider uiTheme={uiTheme}>
                      <Button raised primary text="Ajouter un employé" />
                    </ThemeProvider>
                </View>
            </View>
        );
    }
}




const styles = StyleSheet.create({
  container: {
    //flexDirection: 'row', pour aligner horizontalement 
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  heading: {
    fontSize: 28,
    textAlign: 'center',
    marginTop: 10,
    color: '#F5FCFF',
    fontWeight: 'bold'
  },
  body: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    width: 200,
  },
  tete: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#0050ef',
  },
  text: {
    justifyContent: 'space-between',
    textAlign: 'center',
    marginBottom: 5,
    //color: '#F5FCFF'
  },
  nom:{
      color: 'black'
  },
  image: {
    alignSelf: 'center',
  },
  input: {
    color: '#F5FCFF',
    borderWidth: 1,
    borderColor: '#F5FCFF'
  },
  employe: {
    marginLeft: 20,
    marginRight: 50,
    fontSize: 20,
    color: '#F5FCFF'
  }
});